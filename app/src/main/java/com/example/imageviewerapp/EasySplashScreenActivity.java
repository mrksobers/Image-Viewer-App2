package com.example.imageviewerapp;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import gr.net.maroulis.library.EasySplashScreen;




public class EasySplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Splash Screen for application using easy Splash screen
        EasySplashScreen config = new EasySplashScreen(EasySplashScreenActivity.this)
                .withFullScreen()
                .withTargetActivity(MainActivity.class)
                .withSplashTimeOut(5000)
                .withBackgroundColor(Color.parseColor("#9ee1fc"))
                .withFooterText("@Information Technology Students")
                .withLogo(R.mipmap.ic_launcher_round);

        config.getFooterTextView().setTextColor(Color.parseColor("black"));
        View EasySplash = config.create();
        setContentView(EasySplash);
    }

}
